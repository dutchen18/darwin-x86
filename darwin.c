#include <sys/mman.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define WIDTH 200
#define TPS 60
#define ENTER(r) \
	"push "r"rax\npush "r"rbx\npush "r"rcx\npush "r"rdx\npush "r"rsi\npush "r"rdi\npush "r"rbp\n" \
	"push "r"r8 \npush "r"r9 \npush "r"r10\npush "r"r11\npush "r"r12\npush "r"r13\npush "r"r14\npush "r"r15\n"
#define LEAVE(r) \
	"pop "r"r15\npop "r"r14\npop "r"r13\npop "r"r12\npop "r"r11\npop "r"r10\npop "r"r9 \npop "r"r8 \n" \
	"pop "r"rbp\npop "r"rdi\npop "r"rsi\npop "r"rdx\npop "r"rcx\npop "r"rbx\npop "r"rax\n"
#define ASM(func, pre, post) \
	extern void func##_(); \
	asm(".text\n" #func "_:\nmov %rsp, %rbp\n" ENTER("%") pre "call " #func "\n" post LEAVE("%") "ret\n");

ASM(ump_probe,
	"xchg %rsi, %rdi\n"
	"mov -32(%rdi), %rdi\n"
	"lea -8(%rbp), %rdx\n"
	"lea -16(%rbp), %rcx\n"
	"lea -24(%rbp), %r8\n",)
ASM(ump_kill,
	"xchg %rsi, %rdi\n"
	"mov -32(%rdi), %rdi\n",)
ASM(ump_claim,
	"xchg %rsi, %rdi\n"
	"mov -32(%rdi), %rdi\n",)

struct umpire_t;
struct organism_t;

struct opt_t {
	uint32_t (*max_prot)(uint32_t);
	uint32_t max_size;
	size_t arena_size;

	void (*on_probe)(struct umpire_t *umpire, uint8_t *loc);
	void (*on_kill)(struct umpire_t *umpire, struct organism_t *organism);
	void (*on_claim)(struct umpire_t *umpire, struct organism_t *organism);
};

struct species_t {
	uint32_t id;
	uint32_t size;
	uint32_t origin;
	uint32_t prot_len;
	uint32_t *prot;
	uint8_t *code;
};

struct umpire_t {
	struct opt_t opt;
	struct map_t {
		struct {
			struct umpire_t *umpire;
			void (*probe)(), (*kill)(), (*claim)();
		} api;
		uint8_t arena[1];
	} *map;
	void *rsp;

	struct organism_t *current;
	struct organism_t {
		struct species_t *species;
		uint8_t *loc;
		struct organism_t *next;
	} *organism;
	struct probed_t {
		uint8_t *loc, *bot, *top;
		struct probed_t *next;
	} *probed;
};

uint32_t max_prot_20 (uint32_t species_size) { return 20; }
uint32_t max_prot_10 (uint32_t species_size) { return 10; }
uint32_t max_prot_1_4(uint32_t species_size) { return species_size / 4; }

bool species_new(struct species_t *species, struct opt_t *opt, const char *name) {
	FILE *f = fopen(name, "rb");
	fread(&species->id, sizeof(species->id), 1, f);
	fread(&species->size, sizeof(species->size), 1, f);
	fread(&species->origin, sizeof(species->origin), 1, f);
	fread(&species->prot_len, sizeof(species->prot_len), 1, f);
	species->prot = malloc(sizeof(*species->prot) * species->prot_len);
	species->code = malloc(species->size);
	fread(species->prot, sizeof(*species->prot), species->prot_len, f);
	fread(species->code, 1, species->size, f);
	fclose(f);

	if (species->origin >= species->size) return false;
	for (uint32_t i = 0; i < species->prot_len; ++i)
		if (species->prot[i] >= species->size) return false;
	if (species->size > opt->max_size) return false;
	return true;
}

void species_del(struct species_t *species) {
	free(species->prot);
	free(species->code);
}

void umpire_new(struct umpire_t *umpire, struct opt_t *opt) {
	umpire->opt = *opt;
	size_t size = opt->arena_size + sizeof(umpire->map->api);
	int prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	umpire->map = mmap(NULL, size, prot, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	umpire->map->api.umpire = umpire;
	umpire->map->api.probe = ump_probe_;
	umpire->map->api.kill = ump_kill_;
	umpire->map->api.claim = ump_claim_;
	umpire->current = NULL;
	umpire->organism = NULL;
	umpire->probed = NULL;
}

void umpire_forget_probed(struct umpire_t *umpire) {
	while (umpire->probed != NULL) {
		struct probed_t *tmp = umpire->probed;
		umpire->probed = tmp->next;
		free(tmp);
	}
}

struct organism_t *umpire_get_organism(struct umpire_t *umpire, uint8_t *loc) {
	for (struct organism_t *organism = umpire->organism; organism != NULL; organism = organism->next)
		if (loc >= organism->loc && loc < organism->loc + organism->species->size) return organism;
	return NULL;
}

bool umpire_is_prot(struct umpire_t *umpire, struct organism_t *organism, uint8_t *loc) {
	uint32_t max_prot = umpire->opt.max_prot(organism->species->size);
	if (organism->species->prot_len < max_prot) max_prot = organism->species->prot_len;
	for (uint32_t i = 0; i < max_prot; ++i)
		if (organism->species->prot[i] + organism->loc == loc) return true;
	return false;
}

void umpire_kill_organism(struct umpire_t *umpire, struct organism_t *organism) {
	for (struct organism_t **organism2 = &umpire->organism; *organism2 != NULL;) {
		if (*organism2 == organism) {
			struct organism_t *tmp = *organism2;
			*organism2 = tmp->next;
			umpire->opt.on_kill(umpire, tmp);
			free(tmp);
		} else organism2 = &(*organism2)->next;
	}
}

void umpire_jump_organism(struct umpire_t *umpire, struct organism_t *organism) {
	umpire_forget_probed(umpire);
	umpire->current = organism;
	void *dest = organism->loc + organism->species->origin;
	asm("mov %0, %%rsp\n"
		"mov %1, %%rsi\n"
		"mov %2, %%rdx\n"
		"jmp *%3\n"
		:: "m"(umpire->rsp), "r"(umpire->map->arena), "r"(umpire->opt.arena_size), "r"(dest));
}

void umpire_exterminate_current(struct umpire_t *umpire) {
	struct species_t *species = umpire->current->species;
	for (struct organism_t *organism = umpire->organism; organism != NULL;) {
		struct organism_t *next = organism->next;
		if (organism->species == species)
			umpire_kill_organism(umpire, organism);
		organism = next;
	}
	umpire_jump_organism(umpire, umpire->organism);
}

void umpire_assert_probed(struct umpire_t *umpire, uint8_t *loc) {
	for (struct probed_t *probed = umpire->probed; probed != NULL; probed = probed->next)
		if (loc >= probed->bot && loc < probed->top) return;
	umpire_exterminate_current(umpire);
}

struct organism_t *umpire_try_claim(struct umpire_t *umpire, struct species_t *species, uint8_t *loc) {
	for (struct organism_t *organism = umpire->organism; organism != NULL; organism = organism->next) {
		if (loc >= organism->loc && loc < organism->loc + organism->species->size) return NULL;
		if (organism->loc >= loc && organism->loc < loc + species->size) return NULL;
	}
	struct organism_t *organism = malloc(sizeof(struct organism_t));
	*organism = (struct organism_t) { species, loc, umpire->organism };
	umpire->organism = organism;
	umpire->opt.on_claim(umpire, organism);
	return organism;
}

void umpire_spawn_species(struct umpire_t *umpire, struct species_t *species) {
	while (true) {
		uint8_t *loc = umpire->map->arena + (rand() % (umpire->opt.arena_size - species->size));
		struct organism_t *organism = umpire_try_claim(umpire, species, loc);
		if (organism != NULL) {
			memcpy(organism->loc, species->code, species->size);
			break;
		}
	}
}

void umpire_start_organism(struct umpire_t *umpire, struct organism_t *organism) {
	asm(ENTER("%%")
		"mov %%rsp, %0\n"
		: "=m"(umpire->rsp));
	umpire_jump_organism(umpire, organism);
	asm("ump_done:\n"
		LEAVE("%"));
}

void umpire_del(struct umpire_t *umpire) {
	munmap(umpire->map, umpire->opt.arena_size + sizeof(umpire->map->api));
	umpire_forget_probed(umpire);
	while (umpire->organism != NULL)
		umpire_kill_organism(umpire, umpire->organism);
}

void ump_probe(struct umpire_t *umpire, uint8_t *loc, unsigned long *id, uint8_t **bot, uint8_t **top) {
	if (loc < umpire->map->arena || loc >= umpire->map->arena + umpire->opt.arena_size)
		umpire_exterminate_current(umpire);
	*id = 0;
	*bot = umpire->map->arena;
	*top = umpire->map->arena + umpire->opt.arena_size;
	for (struct organism_t *organism = umpire->organism; organism != NULL; organism = organism->next) {
		uint8_t *beg = organism->loc, *end = organism->loc + organism->species->size;
		if (beg > loc && beg < *top) *top = beg;
		if (end <= loc && end > *bot) *bot = end;
	}
	struct probed_t *probed = malloc(sizeof(struct probed_t));
	*probed = (struct probed_t) { loc, *bot, *top, umpire->probed };
	umpire->probed = probed;
	umpire->opt.on_probe(umpire, loc);
	struct organism_t *organism = umpire_get_organism(umpire, loc);
	if (organism != NULL) {
		*id = organism->species->id;
		if (umpire_is_prot(umpire, organism, loc))
			umpire_jump_organism(umpire, organism);
	}
}

void ump_kill(struct umpire_t *umpire, uint8_t *loc) {
	umpire_assert_probed(umpire, loc);
	struct organism_t *organism = umpire_get_organism(umpire, loc);
	if (organism == NULL) umpire_exterminate_current(umpire);
	if (organism == umpire->current) umpire_exterminate_current(umpire);
	umpire_kill_organism(umpire, organism);
}

void ump_claim(struct umpire_t *umpire, uint8_t *loc) {
	umpire_assert_probed(umpire, loc);
	if (umpire_try_claim(umpire, umpire->current->species, loc) == NULL)
		umpire_exterminate_current(umpire);
}

void draw(char ch, int color, size_t begin, size_t size) {
	for (size_t i = begin; i < begin + size; ++i)
		printf("\033[%lu;%luH\033[40;1;%im%c", (i / WIDTH) + 1, (i % WIDTH) + 1, color, ch);
	fflush(stdout);
}

int get_color(struct species_t *species) {
	uint32_t sum = 0, num = species->id;
	while (num > 0) {
		sum += num % 8;
		num /= 8;
	}
	return sum % 8;
} 

void on_probe(struct umpire_t *umpire, uint8_t *loc) {
}

void on_kill(struct umpire_t *umpire, struct organism_t *organism) {
	struct timespec t = { .tv_sec = 0, .tv_nsec = 1000000000 / TPS };
	while (nanosleep(&t, &t));
	draw(' ', 30, organism->loc - umpire->map->arena, organism->species->size);
}

void on_claim(struct umpire_t *umpire, struct organism_t *organism) {
	struct timespec t = { .tv_sec = 0, .tv_nsec = 1000000000 / TPS };
	while (nanosleep(&t, &t));
	draw('#', get_color(organism->species) + 30, organism->loc - umpire->map->arena, organism->species->size);
}

int comp(const void *e1, const void *e2) {
	const struct species_t *a = e1, *b = e2;
	return a->size > b->size ? -1 : (a->size < b->size ? 1 : 0);
}

int main(int argc, char **argv) {
	struct opt_t opt = { max_prot_1_4, 2000, 10000 };
	opt.on_probe = on_probe;
	opt.on_kill = on_kill;
	opt.on_claim = on_claim;

	int species_len = argc - 1;
	if (species_len == 0)
		return printf("Usage: ./darwin file...\n"), 1;
	struct species_t *species = malloc(sizeof(struct species_t) * species_len);
	struct umpire_t umpire;
	umpire_new(&umpire, &opt);

	struct organism_t *organism = NULL;
	for (int i = 0; i < species_len; ++i)
		if (!species_new(&species[i], &opt, argv[i + 1]))
			return printf("Invalid species %s\n", argv[i + 1]), 1;
	qsort(species, species_len, sizeof(*species), comp);
	printf("\033[1;1H\033[J\033");
	draw(' ', 30, 0, umpire.opt.arena_size);

	srand(time(NULL));
	for (int i = 0; i < species_len; ++i) {
		size_t count = opt.arena_size / 2 / species_len / species[i].size;
		while (count--) umpire_spawn_species(&umpire, &species[i]);
		if (organism == NULL || umpire.organism->species->size < organism->species->size)
			organism = umpire.organism;
	}
	umpire_start_organism(&umpire, organism);

	umpire_del(&umpire);
	for (int i = 0; i < species_len; ++i)
		species_del(&species[i]);
	free(species);
}
