	# umpire calls
	probe = -24
	kill = -16
	claim = -8

	# header
	my_id = 0x00000003
	.int my_id # species id
	.int end - code # code size
	.int origin - code # code origin
	.int (code - prot) / 4 # protected byte count

	# protected bytes
prot:
	.int 0

code:
	.quad 0
origin:
	add %rsi, %rdx
l1:
	mov code(%rip), %rdi
	inc %rdi
	cmp %rdi, %rsi
	cmova %rsi, %rdi
	cmp %rdi, %rdx
	cmovbe %rsi, %rdi
	mov %rdi, code(%rip)
	call *probe(%rsi)
	cmp $my_id, %rax
	je l1
	test %rax, %rax
	je l2
	call *kill(%rsi)
l2:
	sub %rbx, %rcx
	cmp $end - code, %rcx
	jl l1
	mov %rbx, %rdi
	call *claim(%rsi)
	mov $end - code, %rcx
l3:
	lea code(%rip), %rax
	mov (%rax, %rcx), %al
	mov %al, (%rbx, %rcx)
	loop l3
	jmp l1
end:
