	# umpire calls
	probe = -24
	kill = -16
	claim = -8

	# header
	my_id = 0x00000000
	.int my_id # species id
	.int end - code # code size
	.int origin - code # code origin
	.int (code - prot) / 4 # protected byte count

	# protected bytes
prot:
	.int 0, 1, 2, 3 # ...

code:
	# this is a place to store data
origin:
	# execution will start here
	# %rsi = arena base address
	# %rdx = size of arena
	jmp origin
	
	# PROBE example
	# %rsi = arena base address
	# %rdi = location to probe
	call *probe(%rsi)
	# %rax = id of species at location
	# %rbx = bottom of largest free space around location
	# %rcx = top of largest free space around location
	# if you hit a protected byte of another organism control is given to that organism

	# KILL example
	# %rsi = arena base address
	# %rdi = (previously probed!) location to kill
	call *kill(%rsi)
	# organism at location is killed
	# suicide is forbidden

	# CLAIM example
	# %rsi = arena base address
	# %rdi = (previously probed!) location to claim
	call *claim(%rsi)
	# the umpire registers an organism of your species at location
	# it will have the same protected bytes and origin as your current organism
	# you will probably want to copy your code to the new location
end:
