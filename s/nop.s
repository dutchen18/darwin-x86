	# umpire calls
	probe = -24
	kill = -16
	claim = -8

	# header
	my_id = 0x00000001
	.int my_id # species id
	.int end - code # code size
	.int origin - code # code origin
	.int (code - prot) / 4 # protected byte count

prot:

	# this species probes memory until it finds a protected byte
	# the umpire makes sure we lose control after probing a protected byte
	# since this species itself has no protected bytes it will always hit another species
code:
origin:	
	# when execution starts %rsi will have the base address of the arena
	# store it in %rdi as a parameter for PROBE
	mov %rsi, %rdi
	# this loop will probe all bytes in the arena
	# if it hits a protected byte the umpire will take away our control
	# if no protected bytes exist it does an illegal PROBE and gets exterminated
l1:
	call *probe(%rsi)
	inc %rdi
	jmp l1
end:
