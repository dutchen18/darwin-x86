x86_64 recreation of (1961) programming game Darwin.  
https://programminggames.org/Darwin  
In Darwin you write a program (species) in assembly language to compete over a region of memory called the Arena.  
Your species is capable of probing memory for other organisms, killing other organisms, and claiming new space for reproduction.

For writing your own species, take a look at these resources which this project was based on:  
https://corewar.co.uk/darwin/darwin.txt  
https://www.cs.dartmouth.edu/~doug/darwin.pdf  
For implementation details see `s/base.s`

Feel free to message me about the project, if you need help creating a species, or if you just want to say hi.  
Reddit: u/DutChen18  
Discord: DutChen18#7605  
Happy coding!

usage
-----
`./make.sh` builds the umpire and all species in `s/*`  
make sure your terminal is greater than 200x50 before running  
`./darwin file1 files...` runs the umpire with species from the specified files  
the 200x50 box is a live view of the organisms in memory