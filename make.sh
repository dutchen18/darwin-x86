gcc -std=gnu99 darwin.c -g -o darwin -Wall -pedantic
for file in s/*.s; do
	file="${file%.s}"
	as "$file.s" -o "$file.o"
	objcopy "$file.o" -O binary "$file.bin"
	rm "$file.o" 
done